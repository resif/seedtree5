filetree.py				File indexer
myTools.py				Various utilities functions
readmseed.py				Parse miniseed headers from Python 
seedtree5.py				Miniseed file indexer
seedtree-admin				Database schema management
seedtree_config.py.dist			Default configuration file
seedtree_fpart_scan.sh			Prints commands to split a big scan into smaller bits
misc/					various tools and scripts
	initialize_all_schemas.sh       Print commands to initialize database schemas for RESIF networks
	msimetadata2json.sh             Prints miniseed file statistics as JSON
	bynet_to_byyear.py              Converts file paths from bynet to byyear paths
	seedtree_fpart_scan.sh          Prints commands to split a big scan into smaller bits

