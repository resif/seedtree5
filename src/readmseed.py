#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
"""
Tools to read & parse miniseed files.
uses external call to IRIS Miniseed inspector binary.
"""

# python standard lib
import argparse
from datetime import datetime
from collections import namedtuple
import subprocess
import json
import logging
import os
import sys
from StringIO import StringIO

# Path to IRIS msi 
MSI_PATH = os.environ.get('MSI_PATH')

# not used in this version
# MSI_METADATA2JSON = os.environ.get('MSI_METADATA2JSON')
# MSSTATQC_JSON_PATH = os.environ.get ('MSSTATQC_JSON_PATH')

# Named tuple for trace storage
MSEEDTrace = namedtuple ( "MSEEDTrace", "network station location channel quality starttime endtime nsamples samplerate recordlength fileoffset" )

def msi2bulktraces ( filetuple ):
    """
    Read miniseed file and return all its traces as a stringIO
    (ie. ready for usage with psycopg 'copy_from' command)
    filetuple is expected as (fileindex, directory, filename).
    Traces are calculated using a combination of two IRIS msi calls.
    """
    filepath = os.path.join(filetuple[1],filetuple[2])

    # get "msi -O"" output 
    # this is necessary to get segments with block sizes and fileoffset informations, 
    # not provided by "msi -T", see next code block.
    args = [ MSI_PATH, "-O", filepath ]    
    stdout = subprocess.check_output ( args )
    # split by line, remove last line
    lines = stdout.split('\n')
    lines.pop()
    segments = []
    for l in lines:
        columns = l.split()
        offset = columns[0]
        ( network, station, location, channel ) = columns[1].strip(',').split( '_' )
        quality = columns[3].strip(',')
        recordlength = columns[4].strip(',')
        time = datetime.strptime(columns[9], "%Y,%j,%H:%M:%S.%f")
        segments.append ( {"fileoffset":offset,"net":network,"sta":station,"loc":location,"cha":channel,"qual":quality,"recordlength":recordlength,"start":time} )

    # sort segments by file offset position 
    segments = sorted(segments, key=lambda d: (d['fileoffset']))

    #
    # get "msi -T"" output (get a sorted trace list).
    # trace list is then matched with "msi -O" output (see above) 
    # to provide extra informations.
    #
    args = [ MSI_PATH, "-T", "-Q", "-tf", "1", filepath ]    
    stdout = subprocess.check_output ( args )

    # split by line, remove first and last lines
    lines = stdout.split('\n')
    lines.pop(0)
    lines.pop()
    lines.pop()

    # initialize some variables
    bulkinsert = StringIO()
    nbtraces = 0
    firstline = True
    fileindex = filetuple[0]
    
    # parse each line 
    for l in lines:
        columns = l.split()
        ( network, station, location, channel, quality ) = columns[0].split( '_' )
        starttime = columns[1]
        endtime = columns[2]
        samplerate = columns[3]
        nsamples = columns[4]

        search = (item for item in segments if item['net'] == network \
            and item['sta'] == station \
            and item['loc'] == location \
            and item['cha'] == channel \
            and item['qual'] == quality \
            and item['start'] == datetime.strptime(starttime,'%Y-%m-%dT%H:%M:%S.%f')).next()
        # print network,station,location,channel,quality,starttime,"=>", d
        l = ( fileindex, network, station, location, channel, quality,
            starttime, endtime, nsamples, samplerate, search['recordlength'], search['fileoffset'] )
        bulkinsert.write ( ('' if firstline else '\n') + '\t'.join(str(item) for item in l) )
        firstline = False
        nbtraces += 1   

    return ( fileindex, filepath, nbtraces, bulkinsert )

def msi2metadata ( filetuple ):
    """
    FIXME
    read miniseed file, returns synthetic header metadata informations as JSON 
    """
    filepath = os.path.join(filetuple[1],filetuple[2])
    args = [  MSI_METADATA2JSON, filepath ]
    try: 
        return filetuple[0], subprocess.check_output ( args )
    except Exception as e:  
        raise RuntimeError( "problem while running %s : %s" % (' '.join(args), str(e)) )

    
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

if __name__ == "__main__":
    parser = argparse.ArgumentParser ( description = 'Reads a miniseed file, output its traces. This tool is part of Seedtree. Miniseed reader is based on IRIS msi.' )
    parser.add_argument("--bulktraces", help='Print traces (bulk format) for a miniseed file',metavar='FILENAME')
    args = parser.parse_args()

    # send all logs to console
    logging.basicConfig ( stream = sys.stdout, format = '[%(levelname)s] %(message)s', level = logging.DEBUG )
    
    # read miniseed file, print traces
    if args.bulktraces:
        (fileindex, filepath, nbtraces, bulk) = msi2bulktraces ( ('n/a', os.path.dirname ( args.bulktraces ) , os.path.basename ( args.bulktraces ) ) )
        logging.debug("+%d traces in %s" % (nbtraces, args.bulktraces))
        print 'fileindex', ' '.join ( MSEEDTrace._fields )
        print bulk.getvalue()
