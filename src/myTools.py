
import hashlib
import binascii
from datetime import datetime
import dateutil.parser
import itertools
import subprocess
import string

# for memory leak detection
import gc
import inspect
import zlib

# for tabular formatting
FIELD_WIDTH = 18 

def compute_md5sum ( filename ):
  """Computes md5sum for file named 'filename'. Returns hex string"""
  file = open ( filename )
  md5sum = hashlib.md5(file.read()).hexdigest()
  file.close()
  # return (long(md5sum,16))
  return (md5sum)

def compute_md5sum_external ( filename ):
  """same as above but use external md5sum command"""
  proc = subprocess.Popen ( args = [ 'md5sum', filename ], stdout = subprocess.PIPE )
  output = proc.stdout.read()
  return ( output.split()[0] )

def compute_CRC32 (filename):
    """Computes CRC32 checkcum for a file"""
    buf = open(filename,'rb').read()
    return binascii.crc32(buf) & 0xFFFFFFFF

def adler32 (filename):
    """Computes Adler32 checksum for a file"""
    buf = open(filename,'rb').read()    
    return zlib.adler32(buf) & 0xFFFFFFFF
    
def iso8601date ( string ):
  """Check if the argument is a valid ISO 8601 date string.
  will raise a ValueError exception if parse problem occurs"""
  date = datetime.strptime ( string,"%Y-%m-%dT%H:%M" )
  # if parse is ok, return original string
  return ( string )

def iso8601dateTimespan ( string ):
  """Check if the argument is a valid timespan date string.
  will raise a ValueError exception if parse problem occurs"""
  try:
     # if split does not work as expected, raise exception below
    splitted = string.split('~', 1)
    startspan = splitted[0]
    endspan = splitted[1]
  except Exception as exception: raise ValueError
  # will also raise a ValueError exception if cannot parse:
  datetime.strptime ( startspan,"%Y-%m-%dT%H:%M" )
  datetime.strptime ( endspan,"%Y-%m-%dT%H:%M" )
  # if parse is ok, return splitted string
  return ( splitted )

def isvaliddate ( date ):
    """check if argument is a valid date string (from dateutil parser opinion)"""
    try: timestamp = dateutil.parser.parse ( date ) 
    except Exception as exception : raise ValueError
    return date

def grouper(iterable, n, fillvalue=None):
    """Collect data into fixed-length chunks or blocks"""
    # grouper('ABCDEFG', 3, 'x') --> ABC DEF Gxx
    # https://docs.python.org/2/library/itertools.html#recipes
    args = [iter(iterable)] * n
    return itertools.izip_longest(fillvalue=fillvalue, *args)
    
def dump_garbage():
  """memory leak helper
  http://code.activestate.com/recipes/65333-debug-with-garbage-collection/
  http://teethgrinder.co.uk/perm.php?a=Python-memory-leak-detector"""
  # force collection
  print "\nCollecting GARBAGE:"
  gc.collect()
  # prove they have been collected
  print "\nCollecting GARBAGE:"
  gc.collect()
  print "\nGARBAGE OBJECTS:"
  for x in gc.garbage:
    s = str(x)
    if len(s) > 80: s = "%s..." % s[:80]
    print "::", s
    print "        type:", type(x)
    print "   referrers:", len(gc.get_referrers(x))
    try:
      print "    is class:", inspect.isclass(type(x))
      print "      module:", inspect.getmodule(x)  
      
      lines, line_num = inspect.getsourcelines(type(x))
      print "    line num:", line_num
      for l in lines: print "        line:", l.rstrip("\n")
    except Exception as e:  print "Exception: ", e
    print "\n"
	
def tab_format ( line ):
  """make some kind of tabular output with its arguments
  TBD - not really good : a recoder!"""
  return ( ''.join([str(s).center(FIELD_WIDTH) for s in line]) )
  
