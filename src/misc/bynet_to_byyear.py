#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

"""
converts /mnt/auto/archive/data/bynet/xxxx paths    (stdin) 
to /mnt/auto/archive/data/byyear/yyyy SDS paths     (stdout)

example usage:

find /mnt/auto/archive/data/bynet/1A2009/ -type f | bynet_to_byyear.py 
(...)
/mnt/auto/archive/data/byyear/2010/1A/PIDGE/BHE.D/1A.PIDGE.00.BHE.D.2010.249
/mnt/auto/archive/data/byyear/2010/1A/PIDGE/BHE.D/1A.PIDGE.00.BHE.D.2010.250
/mnt/auto/archive/data/byyear/2010/1A/PIDGE/BHE.D/1A.PIDGE.00.BHE.D.2010.251
/mnt/auto/archive/data/byyear/2010/1A/PIDGE/BHE.D/1A.PIDGE.00.BHE.D.2010.252
(...)

"""
import fileinput
import os

for fullpath in fileinput.input():
    fullpath = fullpath.rstrip()
    basename = os.path.basename(fullpath)
    (network,station,location,channel,D,year,ddd) = basename.split('.')
    print os.path.join('/mnt/auto/archive/data/byyear/',year,network,station,channel+'.D',basename)
