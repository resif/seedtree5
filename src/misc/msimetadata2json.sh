#!/bin/bash
#
# reads a single miniseed file with "msi -O -pp", 
# outputs a JSON string with some synthetic/factorized metadata
# outputs is done on a single line

# Requirements:
#   msi : IRIS miniseed inspector
#   jk  : https://stedolan.github.io/jq/
#   /dev/shm  : in-memory filesystem  
# 
#

set -e

MSI="msi -O -pp"
ECHO="echo -n"

[[ $# -ne 1 ]] && echo "Usage : `basename $0` <miniseed_file>" && exit 1

# read miniseed file into an in-memory file
memoryfile=/dev/shm/`uuidgen`.msi
$MSI $1 > $memoryfile

# parse-factorize msi output to JSON
$ECHO -n \{

# print encodings
$ECHO \"encoding\"\:
cat $memoryfile | grep encoding | sort | uniq -c | awk '{print $3 $4}' | jq -R . | jq -s . | tr -d '\n'
$ECHO ","

# print time corrections
$ECHO \"time_correction\"\:
cat $memoryfile | grep "time correction"  | sort | uniq -c | awk '{print $4}' | jq -R . | jq -s . | tr -d '\n'
$ECHO ","

# print record lengths
$ECHO \"record_length\"\:
cat $memoryfile | grep "record length" | sort | uniq -c | awk '{print $4}' | jq -R . | jq -s . | tr -d '\n'
$ECHO ","

# print activity flags
$ECHO \"activity_flags\"\:
cat $memoryfile | grep "activity flags" | sort | uniq -c | awk '{print $4}' | sed 's/\(\[\|\]\)//g' | jq -R . | jq -s . | tr -d '\n'

# close JSON 
$ECHO \}
rm $memoryfile

