
#
# prints on stdout all commands needed for:
#
# - creating all seedtree schemas for each RESIF networks
# - scanning/syncing all files
# - computing all traces and products
#
# this command relies on RESIF Station webservice with extended attributes  
#

# FIXME replace resif-vm38.u-ga.fr with ws.resif.fr

# Get all networks/stations | Grep all full FDSN network code | sort | keep unique networks | Change to lowercase
networks=`wget -q -O - "http://resif-vm38.u-ga.fr/fdsnws/station/1/query?level=station&extendedattributes=true" | grep -oP 'alternateNetworkCodes="\K[^"]*'  | sort | uniq | tr '[:upper:]' '[:lower:]'`

for n in $networks; do
    echo echo Doing network $n
	SCHEMA=_$n
    # following line should be commented out if you don't need schema initialization 
	echo seedtree-admin --create $SCHEMA ${n^^}
	
	# update index with all files 
	echo find \/mnt\/auto\/archive\/data\/bynet\/${n^^} -type f \| bynet_to_byyear.py \| sort \| filetree.py --sync $SCHEMA
	
	# compute all miniseed traces and products
	echo seedtree5.py --sync $SCHEMA
	echo seedtree5.py --computeproducts --force $SCHEMA
	echo 
done


